<?php 
use yii\helpers\Html;
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Repaso 1</title>
    </head>
    <body>
        <?php
        foreach ($fotos as $value) {
            echo Html::img("@web/imgs/$value", ['alt' => 'Foto 1', 'class' => 'imagen']);
        }
        ?>
    </body>
</html>
