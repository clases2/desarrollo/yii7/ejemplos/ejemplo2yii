<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionEjerepaso1() {
        //Llamar una vista que debe mostrar un par de imagenes.
        $fotos = [
            "1.jpg",
            "2.jpg",
            "3.jpg",
            "4.jpg"
        ];
        return $this->render('Repaso1', [
                    "fotos" => $fotos
        ]);
    }

    public function actionEjerepaso2() {
        //Llamar una vista con un titulo y un texto.
        $datos = [
            "titulo" => "Ejemplo de datos en MVC",
            "contenido" => "Este contenido se lo pasa el controlador a la vista."
        ];
        return $this->render('Repaso2', [
                    "datos" => $datos
        ]);
    }

    public function actionEjeform1() {
        return $this->render('Form1');
    }

    public function actionEjeform2() {
        return $this->render('Form2');
    }

}
